package com.example.foodtrip;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class StoreFragment extends Fragment implements View.OnClickListener
{

    public CardView cardbq, cardbqq, cardbt, cardcs, cardfb, cardkk, cardth;
    Toolbar toolbar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_store, container, false);

        toolbar = (Toolbar) v.findViewById(R.id.toolbarr);


        cardbq = (CardView) v.findViewById(R.id.barbequ);
        cardbqq = (CardView) v.findViewById(R.id.banana);
        cardbt = (CardView) v.findViewById(R.id.bal);
        cardcs = (CardView) v.findViewById(R.id.cheese);
        cardfb = (CardView) v.findViewById(R.id.fishbal);
        cardkk = (CardView) v.findViewById(R.id.kwek);
        cardth = (CardView) v.findViewById(R.id.tah);

        cardbq.setOnClickListener(this);
        cardbqq.setOnClickListener(this);
        cardbt.setOnClickListener(this);
        cardcs.setOnClickListener(this);
        cardfb.setOnClickListener(this);
        cardkk.setOnClickListener(this);
        cardth.setOnClickListener(this);

        return v;

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.barbequ:
                intent = new Intent(getActivity(),barbeque.class);
                startActivity(intent);
                break;
            case R.id.banana:
                intent = new Intent(getActivity(),bananaque.class);
                startActivity(intent);
                break;
            case R.id.bal:
                intent = new Intent(getActivity(),balut.class);
                startActivity(intent);
                break;
            case R.id.cheese:
                intent = new Intent(getActivity(),cheesesticks.class);
                startActivity(intent);
                break;
            case R.id.fishbal:
                intent = new Intent(getActivity(),fishball.class);
                startActivity(intent);
                break;
            case R.id.kwek:
                intent = new Intent(getActivity(),kwek.class);
                startActivity(intent);
                break;
            case R.id.tah:
                intent = new Intent(getActivity(),taho.class);
                startActivity(intent);
                break;
        }

    }
}