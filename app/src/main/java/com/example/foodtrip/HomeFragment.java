package com.example.foodtrip;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class HomeFragment extends Fragment implements View.OnClickListener {
    Toolbar toolbar;
    public CardView cardGF , cardzar;
    ImageButton myImageButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_home, container, false);

        toolbar = (Toolbar) v.findViewById(R.id.toolbarr);


        myImageButton = v.findViewById(R.id.my_maps);
        myImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),phil.class);
                startActivity(intent);
            }
        });


        cardGF = (CardView) v.findViewById(R.id.STE);
        cardzar = (CardView) v.findViewById(R.id.BAZ);

        cardGF.setOnClickListener(this);
        cardzar.setOnClickListener(this );

    return v;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case  R.id.STE:
                intent = new Intent(getActivity(),streetstore.class);
                startActivity(intent);
                break;
            case  R.id.BAZ:
                intent = new Intent(getActivity(),bazaar.class);
                startActivity(intent);
                break;

        }

    }
}